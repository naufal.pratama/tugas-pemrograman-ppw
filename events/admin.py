from django.contrib import admin
from .models import EventEntry, Attendant

# Register your models here.
admin.site.register(EventEntry)
admin.site.register(Attendant)