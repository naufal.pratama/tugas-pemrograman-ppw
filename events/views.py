from django.shortcuts import render
from .models import EventEntry, Attendant
from django.http import JsonResponse

# Create your views here.
def events(request):
    all_events = EventEntry.objects.all()
    response = {
        "all_events" : all_events
    }
    return render(request, 'events.html', response)

def register(request, reg_event):
    response = { 'success' : False }
    if request.user.is_authenticated:
        targ_event = None
        for event in EventEntry.objects.all():
            if event.title == reg_event:
                targ_event = event
                break
        if targ_event == None:
            return JsonResponse(response)
        for attendant in targ_event.attendant.all():
            if attendant.email == request.user.email:
                return JsonResponse(response)
        participant = Attendant(username = request.user.username, password = request.user.password, email = request.user.email)
        participant.save()
        EventEntry.objects.all().get(title = reg_event).attendant.add(participant)
        response = { 'success' : True }
        return JsonResponse(response)
    else:   
        return JsonResponse(response)