"""TP_PPW URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.views.generic.base import RedirectView
from login import views

urlpatterns = [
    path('admin/', admin.site.urls),

    path('', RedirectView.as_view(url='/news')),
    path('news/', include('news.urls')),
    path('events/', include('events.urls')),
    path('register-as-member/', include('reg_member.urls')),
    path('register-as-participant/', include('reg_participant.urls')),
    path('registered-events/', include('registeredEvents.urls')),
    path('sign/', include('login.urls')),
    path('about/', include('about.urls')),
    path('', include('social_django.urls', namespace='social')),
    path('google17b56790938cb27b.html', views.verifikasi),
]
