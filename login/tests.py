from django.test import TestCase
from .models import GoogleUser

# Create your tests here.
class LoginUnitTest(TestCase):
    def test_model_can_create_googleuser(self):
        # Creating a new event
        google_user = GoogleUser.objects.create(
            username='hardlexander',
            email='hardlexander@gmail.com'
        )
    
        # Retrieving all available events
        counting_all_available_events = GoogleUser.objects.all().count()
        self.assertEqual(counting_all_available_events, 1)