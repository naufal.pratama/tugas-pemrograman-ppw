from django.db import models

# Create your models here.
class GoogleUser (models.Model):
    username = models.CharField(max_length = 50)
    full_name = models.CharField(max_length = 50)
    email = models.EmailField()