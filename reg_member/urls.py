from django.urls import path
from . import views

app_name = 'reg-member'

urlpatterns = [
    path('', views.memregister),
]
