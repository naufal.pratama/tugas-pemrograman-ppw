from django.apps import AppConfig


class RegMemberConfig(AppConfig):
    name = 'reg_member'
