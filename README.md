
# Tugas Pemrograman PPW

TP1 & TP2 PPW

* * *

## Nama Anggota

1. Hardyin Alexander - *Fitur login*
2. Muhammad Rizqi Agung Prabowo - *Halaman About & Testimoni*
3. Naufal Pratama Tanansyah - *Daftar list event & Daftar ke suatu event*
4. Yusuf Tri Ardho Mulyawan - *Daftar event yang pernah didaftarkan sebelumnya*

## Status

1. Story type: Student Union App
2. Pipeline: [![pipeline status](https://gitlab.com/naufal.pratama/tugas-pemrograman-ppw/badges/master/pipeline.svg)](https://gitlab.com/naufal.pratama/tugas-pemrograman-ppw/commits/master)
3. Coverage:   [![coverage report](https://gitlab.com/naufal.pratama/tugas-pemrograman-ppw/badges/master/coverage.svg)](https://gitlab.com/naufal.pratama/tugas-pemrograman-ppw/commits/master)
4. Heroku link: ppw-e-tp.herokuapp.com
