from django.apps import AppConfig


class RegParticipantConfig(AppConfig):
    name = 'reg_participant'
