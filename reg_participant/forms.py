from django import forms
from django.forms import ModelForm

from .models import EVENT_LIST, Participant

# Create your views here.
class RegParticipantForm(ModelForm):
	class Meta:
		model = Participant
		password = forms.CharField(widget=forms.PasswordInput)
		fields = ['username', 'email', 'password', 'event_list']
		widgets = {
			'username': forms.TextInput(attrs={'placeholder': 'Username ...'}),
			'email': forms.TextInput(attrs={'placeholder': 'Email ...'}),
			'password': forms.PasswordInput(attrs={'placeholder': 'Password ...'}),
		}