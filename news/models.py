from django.db import models

# Create your models here.
class User(models.Model):
	username = models.CharField(max_length=25)
	news_code = models.CharField(max_length=50)

class News(models.Model):
	title = models.CharField(max_length=50)
	date = models.DateField()
	description = models.CharField(max_length=900)
	code = models.CharField(max_length=300)

