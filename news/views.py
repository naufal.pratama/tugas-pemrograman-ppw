from django.shortcuts import render
from .models import News, User

# Create your views here.
def news(request):
	all_news = News.objects.all()
	all_user = User.objects.all()
	response = {
		'news_list': all_news,
		'user_list': all_user,
	}
	return render(request, 'news.html', response)
