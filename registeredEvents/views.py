from django.shortcuts import render
from events.models import EventEntry, Attendant

# Create your views here.
def index_registered_events(request):
	if request.user.is_authenticated:
		registeredEventList = []
		for event in EventEntry.objects.all():
			for attendant in event.attendant.all(): # di attendant ada atribut email
				if attendant.email == request.user.email:
					registeredEventList.append(event)
					break
		response = { 'all_events': registeredEventList }
		return render(request, 'registered-events.html', response)
	else:
		return render(request, 'registered-events.html')
