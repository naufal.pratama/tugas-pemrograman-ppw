from django.urls import path
from . import views

app_name = 'registeredEvents'

urlpatterns = [
    path('', views.index_registered_events),
]
