from django.test import TestCase
from django.test import Client
from django.urls import resolve

from .views import index_registered_events
from events.models import EventEntry, Attendant

import json

# Create your tests here.
class RegisteredEventsUnitTest(TestCase):

    def test_page_url_is_exist(self):
        response = Client().get('/registered-events/')
        self.assertEqual(response.status_code, 200)

    def test_page_using_news_template(self):
        response = Client().get('/registered-events/')
        self.assertTemplateUsed(response, 'registered-events.html')

    def test_page_using_news_func(self):
        found = resolve('/registered-events/')
        self.assertEqual(found.func, index_registered_events)

    def test_if_not_authenticate_html_contains_title(self):
        response = Client().get('/registered-events/')
        response_content = response.content.decode('UTF-8')
        self.assertIn('You must login to see this page', response_content)

    def test_check_login_success(self):
        member = Attendant.objects.create(username='username')
        member.password = 'password'
        member.save()
        login = self.client.login(username='username', password='password')
